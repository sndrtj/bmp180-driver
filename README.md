BMP180-DRIVER
=============

This is a python-based driver for BM180 (and BMP085) temperature and pressure sensors
on Linux.

This is a python 3-only reimplementation of [Adafruit's legacy BMP085 driver](https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/tree/legacy/Adafruit_BMP085).

# Requirements

* Python 3.6+
* [i2c-tools](https://i2c.wiki.kernel.org/index.php/I2C_Tools) with py-smbus
    * On debian-based systems install the `i2c-tools` and `python-smbus` packages.
    * If this isn't available for your system, you can use the `smbus2` package
    from pypi.
    
# Usage

You can instantiate the driver with the following snippet:

```python
from bmp180 import BMP180, PowerModes

driver = BMP180(bus_number=1, address=0x77, mode=PowerModes.STANDARD)
```

Explanation of the arguments:

* `bus_number`: This needs to be the i2c bus number where the device lives. This 
usually is either 0 or 1. For convenience, this package comes with a 
`autodetect_bus_number` function to find the bus number. This argument is **required**.
* `address`: The address on the i2c bus where the BMP180 device lives. This argument
is defaulted to 0x77. If unsure, you can run `i2c-detect -y {bus_number}` which gives
you a table of all addresses.
* `mode`: This sets the power mode used for fetching temperature and pressure readings.
Please see section `Power modes` for a full explanation. This argument is defaulted
to `PowerModes.STANDARD`. 

## Fetching temperature and pressure readings.

After instantiating the driver we must first calibrate the sensor. This has to be done
at least once during the lifetime of the driver. To do so, we can simply run: 

```python
driver.calibrate() 
```

Now we can get the temperature and pressure readings:

```python
temp = driver.get_current_temperature()
pressure = driver.get_current_pressure()
```

Both these methods return an integer. The temperature is in units of 0.1C. The pressure
is in units of 1 Pascal.

## Power modes

The power mode determines the signal to noise ratio and potential number of readings
per second we can do. A higher power mode means a better signal to noise ratio, but
slower speeds. 

Temperature readings are not affected by the power mode, and always return in 4.5 ms,
meaning one could do 222 temperature readings a second at maximum speed.

For pressure readings, see the following table for root-mean-squared noise (RMS),
time taken per reading, and max readings per second. 

| Power mode | RMS noise in Pa | Time per reading | Max readings / s |
| ---------- | --------------- | ---------------- | ---------------- |
| ULTRA_LOW_POWER | 6 | 9 ms | 111 | 
| STANDARD | 5 | 12 ms | 83 | 
| HIGH_RESOLUTION | 4 | 18 ms | 55 | 
| ULTRA_HIGH_RESOLUTION | 3 | 30 ms | 33 |

## Cleanup

After you're done, you should call the `driver.close()` method to clean up the bus.

This an also be done automatically by instantiating the driver as a context manager.
E.g.

```python
from bmp180 import BMP180

with BMP180(1) as driver:
    driver.calibrate()
    driver.get_current_pressure()
```
 
    
# Known limitations

The advanced operation mode is not (yet) supported.
    
    
# Datasheet

Algorithmic details were taken from [this](https://cdn-shop.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf) 
datasheet from Bosch (BMP180's manufacturer).
   