from unittest import mock

import pytest

from bmp180 import BMP180, CalibrationRegisters, PowerModes


test_calibrations = {
    CalibrationRegisters.CAL_AC1: 408,
    CalibrationRegisters.CAL_AC2: -72,
    CalibrationRegisters.CAL_AC3: -14383,
    CalibrationRegisters.CAL_AC4: 32741,
    CalibrationRegisters.CAL_AC5: 32757,
    CalibrationRegisters.CAL_AC6: 23153,
    CalibrationRegisters.CAL_B1: 6190,
    CalibrationRegisters.CAL_B2: 4,
    CalibrationRegisters.CAL_MB: -32768,
    CalibrationRegisters.CAL_MC: -8711,
    CalibrationRegisters.CAL_MD: 2868,
}


@pytest.fixture
def mocked_bmp180():
    """Mock a BMP180 object so we can test the algorithm without connected hardware."""
    with mock.patch.object(BMP180, "__init__", lambda x: None):
        mocked = BMP180()
        mocked._BMP180__calibrations = test_calibrations
        mocked.get_uncorrected_temperature = lambda: 27898
        mocked.get_uncorrected_pressure = lambda: 23843
        mocked.mode = PowerModes.ULTRA_LOW_POWER
        yield mocked


def test_get_temperature(mocked_bmp180):
    temp = mocked_bmp180.get_current_temperature()
    assert temp == 150


def test_get_presssure(mocked_bmp180):
    pressure = mocked_bmp180.get_current_pressure()
    assert pressure == 69964
