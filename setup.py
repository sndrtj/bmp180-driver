from setuptools import setup, find_packages
from pathlib import Path

readme_file = Path(__file__).absolute().parent / Path("README.md")
with readme_file.open() as desc_handle:
    long_desc = desc_handle.read()

setup(
    name="bmp180",
    version="0.9.0",
    description="Python3 driver for BMP180 temperature/pressure sensor",
    long_description=long_desc,
    long_description_content_type="text/markdown",
    author="Sander Bollen",
    author_email="sander@sndrtj.eu",
    python_requires=">=3.6",
    license="BSD-3-clause",
    packages=find_packages(),
    url="https://gitlab.com/sndrtj/bmp180-driver",
    zip_safe=False,
    install_requires=[
        "smbus2"
    ],
    classifiers=[
        "License :: OSI Approved :: BSD License",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Software Development :: Embedded Systems"
        "Topic :: System :: Hardware :: Hardware Drivers",
        "Typing :: Typed"
    ]
)
