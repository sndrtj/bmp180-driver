"""
bmp180 driver for python 3

:copyright: (c) 2019 Sander Bollen
:license: BSD-3-clause
"""
import enum
import time
import pathlib
from typing import Optional
import warnings

try:
    import smbus
except ImportError:
    import smbus2 as smbus


import bmp180.utils as utils


class PowerModes(enum.IntEnum):
    """
    Enum for power modes of the device

    Values correspond to the 'oversampling' parameter per the datasheet.
    """

    ULTRA_LOW_POWER = 0
    STANDARD = 1
    HIGH_RES = 2
    ULTRA_HIGH_RES = 3


class CalibrationRegisters(enum.IntEnum):
    """Calibration registers of the device."""

    CAL_AC1 = 0xAA
    CAL_AC2 = 0xAC
    CAL_AC3 = 0xAE
    CAL_AC4 = 0xB0  # unsigned
    CAL_AC5 = 0xB2  # unsigned
    CAL_AC6 = 0xB4  # unsigned
    CAL_B1 = 0xB6
    CAL_B2 = 0xB8
    CAL_MB = 0xBA
    CAL_MC = 0xBC
    CAL_MD = 0xBE


UNSIGNED_CALC_REGISTERS = [
    CalibrationRegisters.CAL_AC4,
    CalibrationRegisters.CAL_AC5,
    CalibrationRegisters.CAL_AC6,
]


class MeasurementRegisters(enum.IntEnum):
    """Enum for registers of the device"""

    CONTROL = 0xF4
    TEMP_DATA = 0xF6
    PRESSURE_DATA = 0xF6


# values for commands.
READ_TEMP_CMD = 0x2E
READ_PRESSURE_CMD = 0x34


class BMP180(object):
    def __init__(
        self,
        bus_number: int,
        address: int = 0x77,
        mode: PowerModes = PowerModes.STANDARD,
    ):
        """
        Instantiates the driver.

        :param bus_number: the bus number to connect to. This is usually either 0 or 1
        :param address: The address of the device on the bus. This is usually 0x77
        :param mode: the power mode to use. Defaults to standard. Set to a
        higher value for more precise readings, but slower operations.
        """
        self.bus = smbus.SMBus(bus_number)
        self.address = address
        self.mode = mode

        # set all calibrations to 0 by default
        self.__calibrations = {reg: 0 for reg in CalibrationRegisters}

    def _read_unsigned_int16(self, register: int) -> int:
        return utils.i2c_read_unsigned_int16(self.bus, self.address, register)

    def _read_signed_int16(self, register: int) -> int:
        return utils.i2c_read_signed_int16(self.bus, self.address, register)

    def _read_unsigned_int8(self, register: int) -> int:
        return utils.i2c_read_unsigned_int8(self.bus, self.address, register)

    def _write_unsigned_int8(self, register: int, val: int) -> None:
        utils.i2c_write_byte(self.bus, self.address, register, val)

    def calibrate(self) -> None:
        """
        Read calibration data from device.

        This has to go in a specific order. Registers AC4, AC5 and AC6 are unsigned,
        whereas the rest is signed.

        """
        for register in CalibrationRegisters:
            if register in UNSIGNED_CALC_REGISTERS:
                value = self._read_unsigned_int16(register.value)
            else:
                value = self._read_signed_int16(register.value)
            self.__calibrations[register] = value

    def get_uncorrected_temperature(self) -> int:
        """
        Get raw temperature reading from device.

        :return: raw temperature reading in unspecified units
        """
        # write command to control register.
        self._write_unsigned_int8(MeasurementRegisters.CONTROL.value, READ_TEMP_CMD)
        time.sleep(4.5 / 1000)  # sleep 4.5 ms
        # read temp data register
        return self._read_unsigned_int16(MeasurementRegisters.TEMP_DATA.value)

    def get_corrected_temperature(self) -> float:
        """
        Get the corrected temperature.

        The algorithm, including intermediate variable names, are taken from
        datasheet BST-BMP180-DS000-09

        This returns a float, as an intermediate value must be computed for
        get_corrected_pressure. Do NOT rely on the float precision here for
        temperature readings. Please use `.temperature` property for precise readings.

        :return: temperature in units of  0.1C
        """
        ut = self.get_uncorrected_temperature()
        x1 = (ut - self.__calibrations[CalibrationRegisters.CAL_AC6]) * (
            self.__calibrations[CalibrationRegisters.CAL_AC5] / 2 ** 15
        )
        x2 = (self.__calibrations[CalibrationRegisters.CAL_MC] * 2 ** 11) / (
            x1 + self.__calibrations[CalibrationRegisters.CAL_MD]
        )

        b5 = x1 + x2
        temp = (b5 + 8) / 2 ** 4
        return temp  # returns a float?

    def get_uncorrected_pressure(self) -> int:
        """
        Get raw pressure reading from device

        :return: raw temperature reading in unspecified units.
        """
        # write command to control register
        cmd_value = READ_PRESSURE_CMD + (self.mode.value << 6)
        self._write_unsigned_int8(MeasurementRegisters.CONTROL.value, cmd_value)

        # wait, length dependent on mode
        time.sleep(self._pressure_wait())

        # read three registers
        msb = self._read_unsigned_int8(MeasurementRegisters.PRESSURE_DATA.value)
        lsb = self._read_unsigned_int8(MeasurementRegisters.PRESSURE_DATA.value + 1)
        xlsb = self._read_unsigned_int8(MeasurementRegisters.PRESSURE_DATA.value + 2)

        pressure = ((msb << 16) + (lsb << 8) + xlsb) >> (8 - self.mode.value)
        return pressure

    def get_corrected_pressure(self) -> int:
        """
        Get corrected pressure reading

        The algorithm, including intermediate variable names, are taken from
        datasheet BST-BMP180-DS000-09

        :return: Pressure in units of pascal.
        """
        b5 = int((self.get_corrected_temperature() * 2 ** 4) - 8)
        up = self.get_uncorrected_pressure()

        b6 = b5 - 4000
        x1 = int(
            (self.__calibrations[CalibrationRegisters.CAL_B2] * (b6 * b6 / 2 ** 12))
            / 2 ** 11
        )

        x2 = int(self.__calibrations[CalibrationRegisters.CAL_AC2] * b6 / 2 ** 11)
        x3 = x1 + x2
        b3 = (
            (
                (self.__calibrations[CalibrationRegisters.CAL_AC1] * 4 + x3)
                << self.mode.value
            )
            + 2
        ) // 4

        x1 = int(self.__calibrations[CalibrationRegisters.CAL_AC3] * b6 / 2 ** 13)
        x2 = int(
            (self.__calibrations[CalibrationRegisters.CAL_B1] * (b6 * b6 / 2 ** 12))
            / 2 ** 16
        )
        x3 = (x1 + x2 + 2) // 4

        b4 = int(
            self.__calibrations[CalibrationRegisters.CAL_AC4] * (x3 + 32768) / 2 ** 15
        )
        b7 = (up - b3) * (50000 >> self.mode.value)

        if b7 < 0x80000000:
            p = int((b7 * 2) / b4)
        else:
            p = int((b7 / b4) * 2)

        x1 = (p // 2 ** 8) ** 2
        x1 = int((x1 * 3038) / 2 ** 16)
        x2 = int((-7357 * p) / 2 ** 16)

        pressure = p + (x1 + x2 + 3791) // 2 ** 4

        return pressure

    def get_altitude(self, sea_level_pressure: float = 101325) -> float:
        """
        Get the current (estimated) altitude in meters.

        This is dependent on the sea level pressure at your location. The altitude
        is computed from the pressure; it is not directly measured.

        :param sea_level_pressure: the sea level pressure at your location in pascal
        :return: estimated altitude in meters.
        """
        pressure = self.get_corrected_pressure()
        altitude = 44330.0 * (1.0 - pow(pressure / sea_level_pressure, 0.1903))

        return altitude

    def _pressure_wait(self) -> float:
        """Get wait time for pressure reading in seconds"""
        if self.mode == PowerModes.ULTRA_LOW_POWER:
            return 4.5 / 1000
        elif self.mode == PowerModes.STANDARD:
            return 7.5 / 1000
        elif self.mode == PowerModes.HIGH_RES:
            return 13.5 / 1000
        elif self.mode == PowerModes.ULTRA_HIGH_RES:
            return 25.5 / 1000
        else:
            raise ValueError("Unknown power mode.")

    def close(self):
        self.bus.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def get_current_temperature(self) -> int:
        """Get temperature in units of 0.1C"""
        if all(val == 0 for val in self.__calibrations.values()):
            warnings.warn(
                "All calibration values are set to 0. Did you run calibrate()?"
            )

        return int(self.get_corrected_temperature())

    def get_current_pressure(self) -> int:
        """Get temperature in Pascal"""
        if all(val == 0 for val in self.__calibrations.values()):
            warnings.warn(
                "All calibration values are set to 0. Did you run calibrate()?"
            )

        return self.get_corrected_pressure()


def autodetect_bus_number() -> Optional[int]:
    """
    Attempt to autodetect the bus number on Linux systems.

    May return None if we cannot determine the bus number.

    :return: bus number or None if indeterminate.
    """
    # try 20 devices.
    for index in range(20):
        if pathlib.Path(f"/dev/i2c-{index}").exists():
            return index
    return None
