"""
bmp180.utils
~~~~~~~~~~~~

:copyright: (c) 2019 Sander Bollen
:license: BSD-3-clause
"""
try:
    import smbus
except ImportError:
    import smbus2 as smbus


def i2c_read_signed_int8(bus: smbus.SMBus, address: int, register: int) -> int:
    """
    Read an signed int8 from an i2c bus.

    :param bus: SMBUS
    :param address: address of the device in the bus
    :param register: the register to query.
    :return: int with range -128-127
    :raises: IOError when we can't read from the register, address or bus.
    """
    result = bus.read_byte_data(address, register)

    # the inverse of two's complement
    result = -(result & 128) + (result & ~128)

    return result


def i2c_read_unsigned_int8(bus: smbus.SMBus, address: int, register: int) -> int:
    """
    Read an unsigned int8 from an i2c.

    Convenience function.
    :param bus: SMBUS
    :param address: address of the device in the bus
    :param register: the register to query
    :return: int with range 0-255
    :raises: IOError when we can't read from the register, address or bus
    """
    return bus.read_byte_data(address, register)


def i2c_read_signed_int16(bus: smbus.SMBus, address: int, register: int) -> int:
    """
    Read a signed int16 from an i2c bus
    :param bus: SMBUS
    :param address: address of the device in the bus
    :param register: the register to query
    :raises: IOError when we can't read from the register, address or bus
    """
    msb = i2c_read_signed_int8(bus, address, register)
    lsb = i2c_read_unsigned_int8(bus, address, register + 1)

    # shift most significant bits 8 bits up, and add least significant bits.
    return (msb << 8) + lsb


def i2c_read_unsigned_int16(bus: smbus.SMBus, address: int, register: int) -> int:
    """
    Read an unsigned int16 from an i2c bus
    :param bus: SMBUS
    :param address:  address of the device int eh bus
    :param register: the register to query
    :raises: IOError when we can't read from the register, address or bus
    :return int with range 0-65535
    """
    msb = i2c_read_unsigned_int8(bus, address, register)
    lsb = i2c_read_unsigned_int8(bus, address, register + 1)

    # shift most significant bits 8 bits up, and add least significant bits.
    return (msb << 8) + lsb


def i2c_write_byte(bus: smbus.SMBus, address: int, register: int, value: int) -> None:
    """
    Write a byte to a register.
    :param bus: SMBUS
    :param address: address of the device in the bus
    :param register: Register to write value to
    :param value: int with range 0-256
    :return: None
    :raises: ValueError when value is not in range
    :raises: IOError, when we can't access bus, address or register
    """
    if not 0 <= value <= 255:
        raise ValueError("Value is not in range")
    bus.write_byte_data(address, register, value)
